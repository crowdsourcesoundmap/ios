//
//  ViewController.swift
//  Decibel Atlas
//
//  Created by CES_the_reaper on 4/10/17.
//  Copyright © 2017 CES_the_reaper. All rights reserved.
//

import UIKit
import MapKit
import AVKit
import AVFoundation
import CoreData

//import CoreAudio
//import CoreAudioKit
//import ResearchKit

class ViewController: UIViewController,CLLocationManagerDelegate, AVAudioPlayerDelegate, AVAudioRecorderDelegate {
    @IBOutlet weak var Map: MKMapView!
    let locManager = CLLocationManager()
    var levelTimer = Timer()
    var recorder:AVAudioRecorder!
    var lowPassResults: Double = 0.0
    var audioPlayer: AVAudioPlayer?
    var audioRecorder: AVAudioRecorder?
    var count = 0;
    var recommendedCount = 15;
    @IBOutlet weak var TextField: UITextField!
    @IBOutlet weak var DetailsText: UITextView!
    let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
    var currentData:DataBlob?
    let mt:MapTools = MapTools()
    var filemgr:FileManager = FileManager()
    var dirPaths:[URL] = []
    var databasePath:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        filemgr = FileManager.default
        dirPaths = filemgr.urls(for: .documentDirectory,in: .userDomainMask)
        databasePath = dirPaths[0].appendingPathComponent("DADB.db").path
        
        
        Map.delegate = mt
        
        
        
        let fileMgr = FileManager.default
        //let dirPaths = fileMgr.urls(for: .documentDirectory, in: .userDomainMask)
        let soundFileURL = dirPaths[0].appendingPathComponent("sound.caf")
        print(soundFileURL.path)
        let recordSettings =
            [AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue,
             AVEncoderBitRateKey: 16,
             AVNumberOfChannelsKey: 2,
             AVSampleRateKey: 44100.0] as [String : Any]
        
        let audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
        
        do {
            
            try audioRecorder = AVAudioRecorder(url: soundFileURL,
                                                settings: recordSettings as [String : AnyObject])
            audioRecorder?.prepareToRecord()
            audioRecorder?.isMeteringEnabled = true
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        if !filemgr.fileExists(atPath: databasePath as String) {
            
            let contactDB = FMDatabase(path: databasePath as String)
            
            if contactDB == nil {
                print("Error from DB: \(contactDB?.lastErrorMessage())")
            }
            
            if (contactDB?.open())! {
                let sql_stmt = "CREATE TABLE IF NOT EXISTS DADB (ID INTEGER PRIMARY KEY AUTOINCREMENT, DECIBELS DOUBLE, LATITUDE DOUBLE, LONGITUDE DOUBLE, ALTITUDE DOUBLE, ACCURACY DOUBLE, TIME DOUBLE)"
                if !(contactDB?.executeStatements(sql_stmt))! {
                    print("Error from DB: \(contactDB?.lastErrorMessage())")
                } else {
                    print("Database exists")
                }
                /*let insertSQL = "INSERT INTO DADB (DECIBELS,LATITUDE,LONGITUDE,ALTITUDE,ACCURACY,TIME) VALUES(\(currentData!.decibels))"
                 let result = contactDB?.executeUpdate(insertSQL,withArgumentsIn: nil)
                 if !result! {
                 status.text = "Failed to add data"
                 print("Error: \(contactDB?.lastErrorMessage())")
                 } else {
                 status.text = "Data Added"
                 
                 }*/
                contactDB?.close()
            } else {
                print("Error from DB: \(contactDB?.lastErrorMessage())")
            }
        }
        
        
        
        
        
        // Ask for Authorisation from the User.
        self.locManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locManager.delegate = self
            locManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locManager.startUpdatingLocation()
        }
        mt.centerMapOnLocation(location: initialLocation, map: Map, radius: 200)
        let contactDB = FMDatabase(path: databasePath as String)
        
        if contactDB == nil {
            print("Error from DB: \(contactDB?.lastErrorMessage())")
        }

        if (contactDB?.open())! {
            MapTools.circleColor = SoundPersonalizer.getUIColor(for: SoundPersonalizer.getAverage(forArea: initialLocation,db: contactDB!))
            
            SoundPersonalizer.addCirclesFromDB(map: Map, db:contactDB!)
        }
        Map.add(MapTools.makeCircle(latitude:21.282778,longitude:-157.829444,radius:90))
        
        
        //Map.add()
        
        //let myStep = ORKInstructionStep(identifier: "intro")
        //myStep.title = "Welcome to ResearchKit"
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
        recordAudio()
        //levelTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target: self selector: @selector(levelTimerCallback:) userInfo:nil repeats:YES];
        //levelTimer = Timer.scheduledTimer(TimeInterval:1.0,target:self,selector:"levelTimerCallback:", userInfo:nil, repeats: true)
        levelTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(levelTimerCallback), userInfo: nil, repeats: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*override func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
     let locValue:CLLocationCoordinate2D = manager.location!.coordinate
     print("locations = \(locValue.latitude) \(locValue.longitude)")
     }*/
    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        //let decibels:Double =
        //let recorder = AVAudioRecorder()
        //recorder.record(forDuration: TimeInterval(10))
        
        //recordAudio();
        
        
        
        let decibels:Double
            
        if(!(Double(audioRecorder!.averagePower(forChannel: 0))+75 < 0)) {
            decibels = Double(audioRecorder!.averagePower(forChannel: 0))+75
        } else{
            decibels = 0.0
        }
        let altitude:Double = manager.location!.altitude
        let latitude:Double = manager.location!.coordinate.latitude
        let longitude:Double = manager.location!.coordinate.longitude
        let accuracy:Double = manager.location!.horizontalAccuracy
        let time:Double = NSDate().timeIntervalSince1970
        currentData = DataBlob(decibels: decibels, latitude: latitude, longitude: longitude, altitude: altitude, accuracy: accuracy, time: time)
        mt.centerMapOnLocation(location: manager.location!, map: Map, radius: 200)
        print("\(count)|++|\(currentData!.time):  \(currentData!.latitude),\(currentData!.longitude),\(currentData!.accuracy)   \(currentData!.decibels)")
        if (count%recommendedCount==0){
            //if let dbPath = databasePath{
            let contactDB = FMDatabase(path: databasePath as String)
            if currentData!.accuracy < 7 && currentData!.decibels > 0{
                if (contactDB?.open())! {
                    /*let insertSQL = "INSERT INTO DADB (DECIBELS,LATITUDE,LONGITUDE,ALTITUDE,ACCURACY,TIME) VALUES(\(currentData!.decibels),\(currentData!.latitude),\(currentData!.longitude),\(currentData!.altitude),\(currentData!.accuracy),\(currentData!.time))"
                    if !(contactDB?.executeStatements(insertSQL))! {
                        print("Error from DB: \(contactDB?.lastErrorMessage())")
                    } else {
                        var message = ""
                        if currentData!.decibels < 25{
                            message = "Sounds around you are barely audible to inaudible"
                        } else if currentData!.decibels < 50{
                            message = "Sounds around you are in the low range"
                        } else if currentData!.decibels < 75{
                            message = "Sounds around you are in the medium range"
                        } else if currentData!.decibels < 100{
                            message = "Sounds around you are in the loud range"
                        } else{
                            message = "You should wear earplugs to protect your hearing, you are in the deafening range"
                        }
                        //if (contactDB?.open())! {
                        //MapTools.circleColor = SoundPersonalizer.getUIColor(for: SoundPersonalizer.getAverage(forArea: manager.location!,db: contactDB!))
                        MapTools.circleColor = SoundPersonalizer.getUIColor(for: currentData!)
                        //}
                        Map.add(MapTools.makeCircle(latitude: currentData!.latitude,longitude:currentData!.longitude, radius: currentData!.accuracy*2))
                        DetailsText.text = "Location: \(currentData!.latitude),\(currentData!.longitude)\nDecibels:\(currentData!.decibels)\nAltitude:\(currentData!.altitude)\nAccuracy:\(currentData!.accuracy)\n\n\(message)"
                        print("Insert Worked")
                    }
                }
                contactDB?.close()*/
                    SoundPersonalizer.pushData(db: contactDB!,current: currentData!)
                    var message = ""
                    if currentData!.decibels < 25{
                        message = "Sounds around you are barely audible to inaudible"
                    } else if currentData!.decibels < 50{
                        message = "Sounds around you are in the low range"
                    } else if currentData!.decibels < 75{
                        message = "Sounds around you are in the medium range"
                    } else if currentData!.decibels < 100{
                        message = "Sounds around you are in the loud range"
                    } else{
                        message = "You should wear earplugs to protect your hearing, you are in the deafening range"
                    }
                    //if (contactDB?.open())! {
                    //MapTools.circleColor = SoundPersonalizer.getUIColor(for: SoundPersonalizer.getAverage(forArea: manager.location!,db: contactDB!))
                    MapTools.circleColor = SoundPersonalizer.getUIColor(for: currentData!)
                    //}
                    Map.add(MapTools.makeCircle(latitude: currentData!.latitude,longitude:currentData!.longitude, radius: currentData!.accuracy*2))
                    DetailsText.text = "Location: \(currentData!.latitude),\(currentData!.longitude)\nDecibels:\(currentData!.decibels)\nAltitude:\(currentData!.altitude)\nAccuracy:\(currentData!.accuracy)\n\n\(message)"

                }
            }
        }
        
        count += 1;
        
        //stopAudio();
    }
    
    
    func levelTimerCallback(/*timer:Timer*/) {
        audioRecorder?.updateMeters()
        
        //let ALPHA: Double = 0.05
        //var peakPowerForChannel = pow(Double(10), (0.05 * Double(audioRecorder!.peakPower(forChannel: 0))))
        //lowPassResults = ALPHA * peakPowerForChannel + (1.0 - ALPHA) * lowPassResults;
        //if(lowPassResults > 0.95){
          //  NSLog("@Mic blow detected");
        //}
        //NSLog("@Average input: %f Peak input: %f Low pass results: %f", audioRecorder!.averagePower(forChannel: 0), audioRecorder!.peakPower(forChannel: 0), lowPassResults);
    }
    
    func recordAudio() {
        if audioRecorder?.isRecording == false {
            //playButton.isEnabled = false
            //stopButton.isEnabled = true
            audioRecorder?.record()
        }
    }
    func stopAudio() {
        //stopButton.isEnabled = false
        //playButton.isEnabled = true
        //recordButton.isEnabled = true
        
        if audioRecorder?.isRecording == true {
            audioRecorder?.stop()
        } else {
            audioPlayer?.stop()
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        //recordButton.isEnabled = true
        //stopButton.isEnabled = false
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Audio Play Decode Error")
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Audio Record Encode Error")
    }
    
    
}


