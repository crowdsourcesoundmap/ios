//
//  SoundPersonalizer.swift
//  Decibel Atlas
//
//  Created by CES_the_reaper on 4/11/17.
//  Copyright © 2017 CES_the_reaper. All rights reserved.
//

import Foundation
import CoreData
import MapKit
class SoundPersonalizer: NSObject{
    static func pushData(db contactDB:FMDatabase, current currentData:DataBlob){
        let insertSQL = "INSERT INTO DADB (DECIBELS,LATITUDE,LONGITUDE,ALTITUDE,ACCURACY,TIME) VALUES(\(currentData.decibels),\(currentData.latitude),\(currentData.longitude),\(currentData.altitude),\(currentData.accuracy),\(currentData.time))"
        let result = contactDB.executeUpdate(insertSQL,withArgumentsIn:nil)
        if !result {
            print("Failure to Add")
        }
    }
    static func getAverage(forArea location:CLLocation, db contactDB:FMDatabase) -> DataBlob{
        var numberOfRows:Double = 0
        var Decibels:Double = 0
        var Latitude:Double = 0
        var Longitude:Double = 0
        var Altitude:Double = 0
        var Accuracy:Double = 0
        var Time:Double = 0
        let SQL = "SELECT DECIBELS,LATITUDE,LONGITUDE,ALTITUDE,ACCURACY,TIME FROM DADB WHERE LATITUDE BETWEEN \(location.coordinate.latitude+0.1) AND \(location.coordinate.latitude-0.1) AND LONGITUDE BETWEEN \(location.coordinate.latitude+0.1) AND \(location.coordinate.latitude-0.1)"
        //do{
        let results: FMResultSet = contactDB.executeQuery(SQL, withArgumentsIn: nil)
        var cumulativeDB:Double = 0;
        var cumulativeLat:Double = 0;
        var cumulativeLong:Double = 0;
        var cumulativeAlt:Double = 0;
        var cumulativeAcc:Double = 0;
        var cumulativeTime:Double = 0;
        print(results.hasAnotherRow())
        while results.hasAnotherRow(){
            results.next()
            //if(results.double(forColumn: "TIME") < NSDate().timeIntervalSince1970 - 43200){
            numberOfRows += 1
            cumulativeDB += results.double(forColumn: "DECIBELS")
            cumulativeLat += results.double(forColumn: "LATITUDE")
            cumulativeLong += results.double(forColumn: "LONGITUDE")
            cumulativeAlt += results.double(forColumn: "ALTITUDE")
            cumulativeAcc += results.double(forColumn: "ACCURACY")
            cumulativeTime += results.double(forColumn: "TIME")
            print("row processed, \(numberOfRows)")
            //}
        }
        Decibels = cumulativeDB/numberOfRows;
        Latitude = cumulativeLat/numberOfRows;
        Longitude = cumulativeLong/numberOfRows;
        Altitude = cumulativeAlt/numberOfRows;
        Accuracy = cumulativeAcc/numberOfRows;
        Time = cumulativeTime/numberOfRows;
        
        //        }catch let error as NSError{
        //          print("Failure")
        //    }
        return DataBlob(decibels:Decibels,latitude:Latitude,longitude:Longitude,altitude:Altitude,accuracy:Accuracy,time:Time)
    }
    static func getUIColor(for blob:DataBlob) -> UIColor{
        print(blob.decibels)
        if (blob.decibels < 50) {
            print("Green")
            return UIColor.green
        } else if (blob.decibels < 75){
            print("Yellow")
            return UIColor.yellow
        } else {
            print("Red")
            return UIColor.red
        }
    }
    static func addCirclesFromDB(map: MKMapView,db:FMDatabase){
        db.close()
        if (db.open()){
        let SQL = "SELECT DECIBELS,LATITUDE,LONGITUDE,ALTITUDE,ACCURACY,TIME FROM DADB"
        let results: FMResultSet = db.executeQuery(SQL, withArgumentsIn: nil)
        print(results.hasAnotherRow())
        while results.hasAnotherRow(){
            results.next();
            let currBlob = DataBlob(
                decibels: results.double(forColumn: "DECIBELS"),
                latitude: results.double(forColumn: "LATITUDE"),
                longitude: results.double(forColumn: "LONGITUDE"),
                altitude: results.double(forColumn: "ALTITUDE"),
                accuracy: results.double(forColumn: "ACCURACY"),
                time: results.double(forColumn: "TIME")
            )
            //MapTools.circleColor = getUIColor(for: )
        }
        } else {
            print("Error")
        }
    }

}


/*
 
 do {
 try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
 } catch let error as NSError {
 print("audioSession error: \(error.localizedDescription)")
 }
 */
